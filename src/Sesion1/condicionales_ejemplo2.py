#Example of conditionals in Python
if True:
    print("True is always true")

if False:
    print("This message won't be show never")

x = int(input("Say a number: "))
    
if(x>0):
    print("It's natural")

elif(x<0):
    print("It's negative")

else:
    print("It's zero!")

if((x>=2)and(x<=10)):
    print("x belong to close interval [2,10]")

if not((x>2)and(x<10)):
    print("x doesn't belong to open interval (2,10)")
    if((x==2)or(x==10)):
        print("but it's a closure point")
