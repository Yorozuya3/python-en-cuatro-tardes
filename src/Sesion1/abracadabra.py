#Escribe abracadabra de forma triangular

w = "abracadabra"
print(f"{w.center(20)}")
print(f"{w[1:-1].center(20)}")
print(f"{w[2:-2].center(20)}")
print(f"{w[3:-3].center(20)}")
print(f"{w[4:-4].center(20)}")
print(f"{w[5:-5].center(20)}")
