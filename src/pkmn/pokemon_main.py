import pygame
import time

pygame.init()

display_width = 800
display_height = 600

gameDisplay = pygame.display.set_mode((display_width,display_height))
pygame.display.set_caption('A bit Racey')

black = (0,0,0)
green = (139,195,74)

clock = pygame.time.Clock()
crashed = False
gengarimg = pygame.image.load('gengar.png')


def gengar(x,y):
    gameDisplay.blit(gengarimg, (x,y))

x =  (display_width * 0.1)
y = (display_height *0.1)
x_change = 0
y_change = 0
gengar_speed = 0

pygame.mixer.init(200000)
pygame.mixer.music.load('music.wav')
pygame.mixer.music.play()
time.sleep(4)
while not crashed:
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            crashed = True

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                x_change = -5
            elif event.key == pygame.K_RIGHT:
                x_change = 5
            elif event.key == pygame.K_DOWN:
                y_change = -5
            elif event.key == pygame.K_UP:
                y_change = 5
                
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                x_change = 0
            else:
                y_change = 0
                
                
                
    x += x_change
    y += y_change
           
    gameDisplay.fill(green)
    gengar(x,y)
        
    pygame.display.update()
    clock.tick(60)

pygame.quit()
quit()
